package com.example.factories;

import android.content.Intent;
import android.net.Uri;
import android.webkit.*;
import java.io.InputStream;

class GameClient extends WebViewClient {

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        Uri uri = Uri.parse(url);
        if (url.startsWith("file:")) {
            return false;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        view.getContext().startActivity(intent);
        return true;
    }

    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
        Uri uri = request.getUrl();
        if (uri.getPath().endsWith("factories.wasm")) {
            try {
                InputStream inputStream = view.getContext().getResources().getAssets().open("factories.wasm");
                return new WebResourceResponse("application/wasm", "UTF-8", inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return super.shouldInterceptRequest(view, request);
    }

}
