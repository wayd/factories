import { Coord } from './tino'

/** 物品 */
export type Item = {
	/** 名称 */
	name: string
	/** 数量 */
	amount: number
	/** 变化量 */
	delta: number
	/** 容量 */
	capacity: number
	/** 是否选中 */
	selected?: boolean
}

/** 库存 */
export type Storage = {
	/** 位置 */
	coord: Coord
	/** 容量 */
	capacity: Uint32Array
	/** 数量 */
	count: Uint32Array
}

/** 建筑物 */
export type Building = {
	/** ID */
	id: number
	/** 名称 */
	name: string
	/** 描述 */
	desc: string
	/** 每秒消耗 */
	cost: string
	/** 配方id */
	recipeId?: number
	/** 可建造地形 */
	builtOn: number
	/** 建造时间 */
	time: number
}

/** 物品变化 */
export type ItemDelta = [id: number, delta: number]

/** 任务 */
export type Task = {
	/** 名称 */
	name: string
	/** 描述 */
	desc: string
	/** 块号 */
	slot: number
	/** 总时间 */
	time: number
	/** 剩余时间 */
	remainTime: number
	/** 配方id */
	recipeId: number
	/** 流水线乘数 */
	k?: number
	/** 是否暂停 */
	paused?: boolean
	/** 值 */
	value: number
	/** 原值 */
	oldValue: number
	/** 是否选中 */
	selected?: boolean
}

/** 科技 */
export type Technology = {
	/** 名称 */
	name: string
	/** 依赖 */
	prereqs: string[]
	/** 所需时间(s) */
	time: number
}

/** 配方 */
export type Recipe = {
	/** ID */
	id: number
	/** 名称 */
	name: string
	/** 周期 */
	period: number
	/** 配方 */
	deltas: ItemDelta[]
	/** 适用建筑 */
	buildings: Set<number>
	/** 需求科技 */
	reqTechs?: Set<string>
}

/** 统计 */
export type Stats = {
	/** 游戏时长 */
	playTime: number
	/** 累计砍树/挖掘/采石次数 */
	totalCollect: number
	/** 累计建造次数 */
	totalBuild: number
	/** 累计升级次数 */
	totalUpgrade: number
	/** 累计降级/拆除次数 */
	totalDestroy: number
}

/** 世界 */
export type World = {
	/** 存档名称 */
	name: string
	/** 游戏模式 */
	mode: number
	/** 难度 */
	difficulty: number
	/** 地图种子 */
	seed: number
	/** 地图大小 */
	mapSize: number
	/** 创建时间 */
	created: number
	/** 更新时间 */
	updated: number
	/** 选项 */
	opts: number
	/** 地图 */
	map?: Uint8Array
	/** 数据 */
	data: Uint8Array[]
	/** 任务队列 */
	tasks: Task[]
	/** 已解锁科技 */
	techs: Set<string>
	/** 统计 */
	stats: Stats
	/** 是否选中 */
	selected?: boolean
}
