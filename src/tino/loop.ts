/** 每帧回调 */
export type Callback = (dt: number) => void

/** 每帧回调 */
export const updatables = new Set<Callback>()

/** 更新可见元素 */
export function updateOnVisable(el: Element, cb: Callback) {
	const observer = new IntersectionObserver(([entry]) => {
		if (entry.isIntersecting)
			updatables.add(cb)
		else
			updatables.delete(cb)
	})
	observer.observe(el)
	return observer
}
