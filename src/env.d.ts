/// <reference types="vite/client" />

import { Preprocessor } from 'vite-plugin-emt'

declare global {
	declare const emt: Preprocessor, styl: Preprocessor
}

interface ImportMetaEnv {
	readonly VITE_ICON_URL: string
	readonly VITE_WASM_URL: string
}

interface ImportMeta {
	readonly env: ImportMetaEnv
}
