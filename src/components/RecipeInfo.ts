import { CydonElement, define } from 'cydon'
import { names } from '../defs'
import { getRecipes, recipes } from '../recipes'
import { Recipe } from '../type'
import { getRecipe, getTile, setProc } from '../game'

/**
 * @element recipe-info
 * 配方信息
 */
@define('recipe-info')
export class RecipeInfo extends CydonElement {
	_selected = -1
	/** 选中的块号 */
	get selected() {
		return this._selected
	}
	set selected(value) {
		this._selected = value
		this.data._recipeId = getRecipe(value)
		this.data.recipes = getRecipes(getTile(value)[2])
	}

	private _recipeId = -1
	/** 流水线id */
	get recipeId() {
		return this._recipeId
	}
	set recipeId(value) {
		const r = recipes[value]
		setProc(this.selected, value, r.period)
		this._recipeId = value
	}

	/** 配方 */
	recipes: Recipe[] = []

	/** 原料 */
	get input() {
		const r = recipes[this.recipeId]
		return r && r.deltas.filter(d => d[1] < 0)
			.map(d => -d[1] + names[d[0]])
			|| []
	}

	/** 产物 */
	get output() {
		const r = recipes[this.recipeId]
		return r && r.deltas.filter(d => d[1] > 0)
			.map(d => d[1] + names[d[0]])
			|| []
	}

	/** 周期 */
	get period() {
		const r = recipes[this.recipeId]
		return r && r.period || 0
	}

	connectedCallback() {
		// @unocss-include
		this.innerHTML = emt`[c-show=recipes.length]
	h2{配方信息}
	.form-control.max-w-xs
		label.label
			span.label-text{选择配方}
		select.input.input-bordered.input-sm.join-item[c-model=recipeId title=配方]
			template[c-for="r, i; recipes"]
				option[value=$r.id]{$r.name}
	dl
		dt{原料}dd{$input}
		dt{产物}dd{$output}
		dt{周期}dd{$period}`
		super.connectedCallback()
	}
}

declare global {
	interface HTMLElementTagNameMap {
		'recipe-info': RecipeInfo
	}
}