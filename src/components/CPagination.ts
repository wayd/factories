import { CydonElement, define } from 'cydon'

/**
 * @element c-pagination
 * 分页器
 */
@define('c-pagination')
export class CPagination extends CydonElement {
	/** 每页条目数 */
	perPages = [5, 10, 20, 50, 100]
	perPage = 10
	/** 当前页码 */
	pageNum = 1
	/** 总条目数 */
	total = 0

	/** 总页数 */
	get totalPage() {
		return Math.ceil(this.total / this.perPage)
	}

	connectedCallback() {
		// @unocss-include
		this.innerHTML = emt`.join
		button.join-item.btn.btn-sm[@click=pageNum=1 title=首页 .disabled=pageNum==1]{«}
		button.join-item.btn.btn-sm[@click=pageNum-- title=上一页 .disabled=pageNum==1]{&lt;}
		select.join-item.input.input-bordered.input-sm[c-model=perPage title=每页显示]
			template[c-for="n; perPages"]
				option[value=$n .selected=perPage==n]{$n}
		span.join-item.bg-base-200.p-btn
			{per page \${(pageNum-1)*perPage+!!total}-\${Math.min(pageNum*perPage,total)} / $total}
		.join-item.bg-base-200
			input.input.input-bordered.input-sm.w-9[type=number c-model.lazy=pageNum
				min=1 max=$totalPage title=跳至指定页]
		button.join-item.btn.btn-sm[@click=pageNum++ title=下一页 .disabled=pageNum==totalPage]{&gt;}
		button.join-item.btn.btn-sm[@click=pageNum=totalPage title=末页 .disabled=pageNum==totalPage]{»}`
		super.connectedCallback()
	}
}

declare global {
	interface HTMLElementTagNameMap {
		'c-pagination': CPagination
	}
}