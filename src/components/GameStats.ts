import { CydonElement, define } from 'cydon'

/**
 * @element game-stats
 * 游戏统计
 */
@define('game-stats')
export class GameStats extends CydonElement {
	/** 已解锁科技 */
	techs = new Set<string>()
	/** 统计 */
	stats = <Stats>{}

	connectedCallback() {
		// @unocss-include
		this.innerHTML = emt`.form-control.justify-center
			.stats.shadow.overflow-hidden
				.stat.place-items-center
					.stat-title{游戏时长(s)}
					.stat-value{$stats.playTime}
				.stat.place-items-center
					.stat-title{已解锁科技}
					.stat-value.text-primary{$techs.size}
				.stat.place-items-center
					.stat-title{累计砍树/挖掘/采石次数}
					.stat-value.text-info{$stats.totalCollect}
			.stats.shadow.overflow-hidden
				.stat.place-items-center
					.stat-title{累计建造次数}
					.stat-value.text-success{$stats.totalBuild}
				.stat.place-items-center
					.stat-title{累计升级次数}
					.stat-value.text-secondary{$stats.totalUpgrade}
				.stat.place-items-center
					.stat-title{累计降级/拆除次数}
					.stat-value.text-error{$stats.totalDestroy}`
		super.connectedCallback()
	}
}

declare global {
	interface HTMLElementTagNameMap {
		'game-stats': GameStats
	}
}
