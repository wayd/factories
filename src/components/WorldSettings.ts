import { CydonOf, define } from 'cydon'
import { pickName } from '../util'

/**
 * @element world-settings
 * 世界设置
 */
@define('world-settings', { extends: 'dialog' })
export class WorldSettings extends CydonOf(HTMLDialogElement) {
	/** 存档名称 */
	name = ''
	/** 游戏模式 */
	mode = 1
	/** 难度 */
	difficulty = 1
	/** 地图种子 */
	seed = 0
	/** 地图大小 */
	mapSize = 64
	/** 更新时间 */
	updated = 0
	/** 统计 */
	stats = <Stats>{}

	techs = new Set

	/** 随机名称 */
	pickName(e: Event) {
		e.preventDefault()
		this.name = pickName()
	}
}

declare global {
	interface HTMLElementTagNameMap {
		'world-settings': WorldSettings
	}
}
