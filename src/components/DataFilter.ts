import { CydonElement, define } from 'cydon'

/**
 * 筛选器
 */
export type Filter = {
	/** 列名 */
	key: string
	/** 筛选操作 */
	op: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
	/** 筛选值 */
	value: string
	/** 逻辑操作 */
	conj: 0 | 1 | 2
	/** 是否有效 */
	valid: boolean
}

/**
 * 排序设置
 */
export type SortSetting = {
	/** 排序键 */
	index: number
	/** 排序方向：1值升序，2值降序，3长度升序，4长度降序，5随机 */
	dir: 1 | 2 | 3 | 4 | 5
}

/**
 * 数据筛选器
 * @element data-filter
 */
@define('data-filter')
export class DataFilter extends CydonElement {
	/** 筛选器标签 */
	filtersTab!: HTMLDetailsElement
	/** 键名 */
	keys!: string[]
	/** 列名 */
	columns!: string[]
	/** 排序键 */
	sortKeys: SortSetting[] = []

	get isLastFilter() {
		return this.i == this.filters.length - 1
	}

	/** 筛选器 */
	filters: Filter[] = []

	i!: number
	f!: Filter

	connectedCallback() {
		// @unocss-include
		this.innerHTML = emt`ul.menu.bg-base-200.rounded-box
		li
			details[ref=filtersTab]
				summary{筛选器}
				ul
					template[c-for="f, i; filters"]
						li
							.block.join
								select.input.input-bordered.input-sm.join-item[c-model=f.key title=筛选列
								.disabled=f.op<0]
									template[c-for="col, i; columns"]
										option[value="\${keys[i]}"]{$col}
								select.input.input-bordered.input-sm.join-item[c-model=f.op title=筛选操作]
									option[value=0]{等于}
									option[value=1]{不等于}
									option[value=2]{大于}
									option[value=3]{小于等于}
									option[value=4]{大于等于}
									option[value=5]{小于}
									option[value=6]{包含}
									option[value=7]{不包含}
									option[value=8]{匹配正则}
									option[value=9]{不匹配正则}
								input.join-item.input.input-bordered.input-sm.w-24[:class="input-error: !f.valid; hidden: f.op==8 || f.op==9"
									c-model.lazy=f.value @change.capture=checkValidity title=筛选 type=search]
								select.join-item.input.input-bordered.input-sm[c-model=f.conj title=连接词 .disabled=isLastFilter]
									option[value=0]{且}
									option[value=1]{或}
								button.join-item.btn.btn-primary.btn-sm[@click=deleteFilter(i) title=删除筛选器]{×}
				button.btn.btn-primary.btn-sm[:class="hidden: filters.length==9" @click=addFilter() title=添加筛选器]{+}
				button.btn.btn-error.btn-sm[:class="hidden: filters.length==0"
				@click=clearFilters title=清空筛选器]{清空筛选器}`
		super.connectedCallback()
		// 展开筛选器时添加筛选器
		this.filtersTab.addEventListener('toggle', e => {
			if ((<HTMLDetailsElement>e.target).open && !this.filters.length)
				this.addFilter(6)
		})
	}

	checkValidity(e: Event) {
		try {
			if (this.f.op > 1)
				RegExp(this.f.value);
			(<HTMLInputElement>e.target).setCustomValidity('')
		} catch (_) {
			(<HTMLInputElement>e.target).setCustomValidity('正则表达式错误')
		}
	}

	//#region 筛选器
	/** 添加筛选器 */
	addFilter(op: Filter['op'] = 0) {
		this.filters.push({
			key: this.keys[0],
			op,
			value: '',
			conj: 0,
			valid: true
		})
	}

	/** 删除筛选器 */
	deleteFilter(index: number) {
		this.filters.splice(index, 1)
	}

	/** 清空筛选器 */
	clearFilters() {
		this.filters = []
	}
	//#endregion
	//#region 排序器
	/** 添加排序器 */
	addSortKey() {
		this.sortKeys.push({ index: 0, dir: 1 })
	}

	/** 删除排序器 */
	deleteSortKey(index: number) {
		this.sortKeys.splice(index, 1)
	}

	/** 清空排序器 */
	clearSortKeys() {
		this.sortKeys = []
	}
	//#endregion
}

declare global {
	interface HTMLElementTagNameMap {
		'data-filter': DataFilter
	}
}