import Inspect from 'vite-plugin-inspect'
import progress from 'vite-plugin-progress'
import emt, { inlineStylus } from 'vite-plugin-emt'
import { presetAttributify, presetMini } from 'unocss'
import transformerDirective from '@unocss/transformer-directives'
import Unocss from 'unocss/vite'
import { presetDaisy } from 'unocss-preset-daisy'

function removeCrossorigin() {
	return {
		name: 'remove crossorigin',
		transformIndexHtml(html: string) {
			return html.replace(/ crossorigin/g, '')
		},
	}
}

export default ({ mode }) => ({
	root: 'src',
	base: './',
	publicDir: '../public',
	build: {
		emptyOutDir: true,
		outDir: '../dist',
		rollupOptions: {
			input: {
				main: './src/index.html',
				game: './src/game.html',
			},
		},
		target: 'esnext',
		modulePreload: {
			polyfill: false
		}
	},
	plugins: [
		emt({
			paths: ['tpl'],
			writeHtml: true,
		}),
		removeCrossorigin(),
		inlineStylus(),
		Unocss({
			preflights: [],
			presets: [
				presetAttributify(),
				presetMini({
					dark: 'media'
				}),
				presetDaisy()
			],
			transformers: [
				transformerDirective({ varStyle: false })
			],
		}),
		Inspect(),
		progress,
	],
	optimizeDeps: {
		include: ['cydon'],
		esbuildOptions: {
			target: 'esnext'
		}
	},
})
