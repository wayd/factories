# 代码规范

## 命名规范
不使用C语言的方式声明枚举，例如
```d
enum {
	HASH_EMPTY = 0,
	HASH_DELETED = 0x1
}
```

改为
```d
enum Hash {
	empty = 0,
	deleted = 0x1
}
```

## 结构规范

若非必要，对象均使用贫血模型

1. Entity中不能有行为
2. Component只能是POD类型
3. System一般使用单例模式
4. util中所有函数必须不依赖其他模块

## 模块结构
- map：地图生成、地形
- tile：图块、气候
- building：建筑
- factory：工厂，有物品转化能力的建筑

- tino：引擎
  - idb：存储

## 纹理设置
| 编号 | 说明     |
| ---- | -------- |
| 0    | 雪碧图   |
| 1    | 地图数据 |
| 2    | 常量表 |

## 图层顺序
1. 建筑物
2. 植被（仅lowland和highland有）
3. 地形

## 地图相关

1. 坐标与块号对应关系：`slot = x + y * mapWidth`，其中`mapWidth`为最大地图宽度

## 生产
- 地图：大小64x64，每格至多有一个建筑，不允许重叠
- 物质状态：分为固体、液体、气体、电能、信息五种，液体和气体统称流体
- 建筑：指人工建筑的所有东西，可分为仓库、工厂、连接器三大类
- 仓库：有物品存储能力的建筑，按可存储物质状态分为固体贮藏、液体贮藏、气体贮藏、电能贮藏、信息贮藏，不相连的仓库相互独立
- 工厂：有物品转化能力的建筑，一个工厂可以有多种不同的生产线，但每个工厂至多同时运行一种生产线，必须与仓库相连才能生产
- 连接器：连接仓库与工厂的建筑，分为道路、管道、电线、光纤四种，分别传输固体、流体、电能、信息，连接器只能与仓库、工厂或同种连接器相连，若工厂的原料或产物中没有对应的物质状态，则不能与相应连接器相连

- 配方：可用物品增量数组表示，若配方中包含增量为0的物品，表示运行这个配方的工厂可以连接相应物品的状态的连接器

结构示例：
```ts
[
    ItemDelta(CuSO4,-1),
    ItemDelta(Fe,-1),
    ItemDelta(FeSO4,1),
    ItemDelta(Cu,1)
]
```
若原料和产物有相同物品，表示生产过程中需要该物品，但不消耗物品

- 生产线：拥有配方recipe，乘数k，步骤step，周期period四个属性，当(tick+step)%period==0完成一次生产周期，其中tick为游戏时间
完成一个生产周期时消耗k倍原料，生产k倍产品，任一原料不足时会降低k的值，直至暂停(k=0)，暂停时每tick，step-1
- 建筑等级：设等级为l，对于仓库，容量=$2^{l-1}$倍1级时的容量，对于工厂，最大生产线乘数$maxK=2^{l-1}$，对于连接器，寿命为1级时的l倍
- 生产条件：原料充足、产物剩余容量充足、配套设施齐全
- 工厂运行效率：k/maxK，period=0表示手动暂停，k=0表示因不满足生产条件而被动暂停
