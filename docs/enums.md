# Factories

```d
/// 最大仓库数量
enum maxStorages = 256;
```

## Status枚举
状态

| 字段             | 中文             | 说明 |
| ---------------- | ---------------- | ---- |
| ok               | 操作成功         |      |
| buildingNotExist | 建筑不存在       |      |
| tooManyStorages  | 仓库数量达到上限 |      |
| storageNotExist  | 仓库不存在       |      |
| queueFull        | 队列已满         |      |
| lack             | 资源不足         |      |
| storageFull      | 库存已满         |      |
| recipeNotExist   | 配方不存在       |      |
| paused           | 已暂停           |      |
| inProgress       | 生产中           |      |

## StorageType枚举
贮藏方式
| 字段        | 中文            | 说明 |
| ----------- | --------------- | ---- |
| solid       | 固体贮藏        |      |
| liquid      | 液体贮藏        |      |
| gas         | 气体贮藏        |      |
| cold        | 冷藏            |      |
| cryogenic   | 低温贮藏(<120K) |      |
| corrosive   | 腐蚀性的        |      |
| radioactive | 放射性材料贮藏  |      |
| electricity | 电能            |      |
| antimatter  | 反物质          |      |

## Items枚举
物品

| 字段             | 中文       | 说明                     |
| ---------------- | ---------- | ------------------------ |
| stone            | 石头       |                          |
| limestone        | 石灰石     |                          |
| log              | 原木       |                          |
| plank            | 木板       |                          |
| water            | 水         | 淡水                     |
| ice              | 冰         |                          |
| coolant          | 冷却液     |                          |
| clay             | 黏土       | 砖的原料                 |
| kaolin           | 高岭土     | 瓷的原料                 |
| brick            | 砖         |                          |
| porcelain        | 瓷         |                          |
| sand             | 沙子       |                          |
| salt             | 盐         |                          |
| coal             | 煤         |                          |
| sulphur          | 硫         |                          |
| crystal          | 水晶矿     |                          |
| diamond          | 钻石       |                          |
| hydrogen         | 氢         |                          |
| gold             | 金         |                          |
| platinum         | 铂         |                          |
| silver           | 银         |                          |
| silverOre        | 银矿石     |                          |
| copper           | 铜         |                          |
| copperOre        | 铜矿石     |                          |
| iron             | 铁         |                          |
| ironOre          | 铁矿石     |                          |
| lead             | 铅         |                          |
| leadOre          | 铅矿石     |                          |
| zinc             | 锌         |                          |
| zincOre          | 锌矿石     |                          |
| titaniumOre      | 钛矿石     |                          |
| titanium         | 钛         |                          |
| uranium          | 铀         |                          |
| depletedUranium  | 贫铀       |                          |
| uraniumOre       | 铀矿石     |                          |
| graphite         | 石墨       |                          |
| hydrocarbon      | 烃         |                          |
| oxyOrganics      | 含氧有机物 | 包含醇、酯等有机化工原料 |
| fertilizer       | 化肥       | 可用于加快树木生长速度   |
| sulfuricAcid     | 硫酸       |                          |
| nitricAcid       | 硝酸       |                          |
| ammonia          | 氨         |                          |
| paper            | 纸         |                          |
| alkali           | 碱         |                          |
| soda             | 纯碱       |                          |
| gunpowder        | 火药       |                          |
| TNT              | 烈性炸药   |                          |
| fuelRod          | 核燃料棒   |                          |
| carbonSteel      | 碳素钢     |                          |
| siliconSteel     | 硅钢       |                          |
| crudeOil         | 原油       |                          |
| LNG              | 液化天然气 |                          |
| concrete         | 混凝土     |                          |
| plastic          | 塑料       |                          |
| AlAlloy          | 铝合金     |                          |
| TiAlloy          | 钛合金     |                          |
| glass            | 玻璃       |                          |
| fiberglass       | 玻璃纤维   |                          |
| silicon          | 硅         |                          |
| germanium        | 锗         |                          |
| rareEarth        | 稀土       |                          |
| rubber           | 橡胶       |                          |
| silastic         | 硅橡胶     |                          |
| cable            | 电缆       |                          |
| PCB              | 电路板     |                          |
| wafer            | 晶圆       |                          |
| chip             | 芯片       |                          |
| slag             | 矿渣       |                          |
| solidWaste       | 固体废料   |                          |
| radioactiveWaste | 放射性废料 |                          |
| superconductor   | 超导体     |                          |
| aerogel          | 气凝胶     |                          |
沥青

## Terrain枚举
前8个为地形类型，后8个为植被类型
| 字段           | 中文       | 说明                   |
| -------------- | ---------- | ---------------------- |
| lowland        | 低地       | 坡度=0，海拔<=7        |
| highland       | 高地       | 坡度=0，海拔>7         |
| mountain       | 山地       | 坡度=1，海拔<=7        |
| peak           | 山顶       | 坡度=1，海拔>7         |
| cliff          | 沟谷       | 坡度>1                 |
| sand           | 沙滩       | 海拔=0                 |
| shallows       | 浅水       | 海拔=-1                |
| deepwater      | 深水       | 海拔<-1              |
| tundra         | 苔原       | 2<温度<=4，湿度>7      |
| steppe         | 草地       | 4<温度<=7，4<湿度<=7   |
| marsh          | 湿地       | 温度>7，湿度>13        |
| coniferous     | 针叶林     | 4<温度<=7，湿度>7      |
| deciduous      | 落叶阔叶林 | 7<温度<=9，7<=湿度<13  |
| evergreen      | 常绿阔叶林 | 9<温度<=11，湿度>7     |
| sclerophyllous | 常绿硬叶林 | 11<温度<=13，4<湿度<=7 |
| forest         | 雨林       | 温度>13，11<=湿度<13   |

## Resource枚举
资源，除wood外其余不可再生
| 字段        | 中文   | 说明 |
| ----------- | ------ | ---- |
| wood        | 原木   |      |
| coalMine    | 煤矿   |      |
| copperMine  | 铜矿   |      |
| ironMine    | 铁矿   |      |
| uraniumMine | 铀矿   |      |
| paragenesis | 共生矿 |      |
| REO         | 稀土矿 |      |

开采资源的

海拔和坡度决定地形，温湿度决定植被（类型和颜色），通常相邻块海拔差越小，温湿度差距越小

## Soil枚举
土壤类型
| 字段     | 中文 | 说明                  |
| -------- | ---- | --------------------- |
| none     | 裸地 | 其他条件              |
| sandy    | 沙土 | 2<温度<=7，4<湿度<=7  |
| drab     | 褐土 | 温度>7，4<湿度<=7     |
| laterite | 红土 | 温度>7，湿度>7        |
| loess    | 黄土 | 2<温度<=7，7<湿度<=11 |
| black    | 黑土 | 2<温度<=7，湿度>11    |

图块类型由海拔、温湿度决定

其他：
遗迹

- 木材产量与温度、湿度、太阳能有关
- 水力发电量与落差（两边海拔差）正相关
- 风力发电量，空气密度，空气密度则取决于气压和温度

运行效率从低到高依次表现为红、黄、绿

BUFF

木工房

## Buildings:ubyte枚举
建筑物
storage
| 字段                        | 中文           | 值  | 说明                               |
| --------------------------- | -------------- | --- | ---------------------------------- |
| road                        | 公路           | 16  |                                    |
| railway                     | 铁路           | 17  |                                    |
| bridge                      | 桥             |     |                                    |
| railwayBridge               | 铁路桥         | 20  |                                    |
| tunnel                      | 隧道           | 67  |                                    |
| railwayTunnel               | 铁路隧道       | 71  |                                    |
| vacuumPipe                  | 真空管道       |     |                                    |
| warehouse                   | 仓库           | 32  |                                    |
| garage                      | 车库           |     |                                    |
| stonePit                    | 石坑           |     |                                    |
| stope                       | 采矿场         |     |                                    |
| woodcutterHut               | 伐木场         |     |                                    |
| workshop                    | 小作坊         |     | 生产火药、玻璃纤维、化肥           |
| kiln                        | 窑             |     | 生产砖和瓷                         |
| saltField                   | 盐田           |     | 生产盐                             |
| petrolStation               | 加油站         |     |                                    |
| windmill                    | 风车           |     |                                    |
| waterwheel                  | 水车           |     |                                    |
| hydropowerStation           | 水电站         |     |                                    |
| storagePowerStation         | 蓄能电站       |     |                                    |
| tidalPowerStation           | 潮汐电站       |     | 只能建在海岸上                     |
| thermalPowerPlant           | 火电站         |     |                                    |
| geothermalPowerStation      | 地热电站       |     |                                    |
| substation                  | 变电站         |     | 用于远距离输电                     |
| smelter                     | 冶炼厂         |     |                                    |
| pylon                       | 电缆塔         |     | 用于输电                           |
| cableFactory                | 电缆厂         |     |                                    |
| ironFoundry                 | 铸铁厂         |     |                                    |
| stoneQuarry                 | 采石场         |     |                                    |
| sandQuarry                  | 采沙场         |     |                                    |
| woodFactory                 | 木材厂         |     |                                    |
| paperMill                   | 造纸厂         |     |                                    |
| brickyard                   | 砖厂           |     | 生产砖                             |
| sulfuricAcidPlant           | 硫酸厂         |     |                                    |
| nitricAcidPlant             | 硝酸厂         |     |                                    |
| ammoniaPlant                | 氨厂           |     |                                    |
| sodaPlant                   | 纯碱厂         |     |                                    |
| ceramicFactory              | 陶瓷厂         |     | 生产瓷                             |
| glassworks                  | 玻璃厂         |     | 生产玻璃、钢化玻璃、玻璃纤维       |
| desalinationPlant           | 海水淡化厂     |     | 生产水、盐                         |
| refinery                    | 炼油厂         |     | 生产有机化工原料和沥青             |
| moldFactory                 | 模具厂         |     |                                    |
| rectifyingColumn            | 精馏塔         |     |                                    |
| chemicalPlant               | 化工厂         |     |                                    |
| fertilizerPlant             | 化肥厂         |     |                                    |
| plasticsPlant               | 塑料厂         |     |                                    |
| coolingTower                | 冷却塔         |     |                                    |
| steelworks                  | 钢铁厂         |     | 生产碳素钢、硅钢                   |
| alloyPlant                  | 合金厂         |     | 生产铝合金、钛合金                 |
| rubberPlant                 | 橡胶厂         |     | 生产橡胶                           |
| siliconFactory              | 硅厂           |     |                                    |
| siliconeRubberFactory       | 硅橡胶厂       |     | 生产硅橡胶                         |
| concretePlant               | 混凝土厂       |     |                                    |
| iceFactory                  | 制冰厂         |     |                                    |
| recyclingPlant              | 回收厂         |     |                                    |
| landfill                    | 垃圾填埋场     |     |                                    |
| gasPowerPlant               | 天然气发电厂   |     |                                    |
| wasteIncinerationPowerPlant | 垃圾焚烧发电厂 |     |                                    |
| battery                     | 蓄电池         | 115 |                                    |
| pool                        | 水池           | 34  |                                    |
| reservoir                   | 水库           | 68  |                                    |
| tank                        | 储罐           | 64  |                                    |
| fluidPipe                   | 流体管道       | 21  |                                    |
| library                     | 图书馆         |     | 存放生产资料                       |
| weatherStation              | 气象站         |     |                                    |
| oilRig                      | 石油钻塔       |     |                                    |
| gravityAccumulator          | 重力储能装置   |     |                                    |
| physicsLab                  | 物理实验室     |     |                                    |
| chemicalLab                 | 化学实验室     |     | 可少量生产硫酸、硝酸、化肥、火药等 |
| bioLab                      | 生物实验室     |     |                                    |
| nuclearPhysicsLab           | 核物理实验室   |     |                                    |
| solarPanel                  | 太阳能电池板   |     |                                    |
| biomassPowerPlant           | 生物电站       |     |                                    |
| photothermalPowerStation    | 光热电站       |     |                                    |
| nuclearPowerPlant           | 核电站         |     |                                    |
| electronicsFactory          | 电子厂         |     |                                    |
| fab                         | 芯片厂         |     |                                    |
| dataCenter                  | 数据中心       |     | 存放生产资料                       |

pipeline

## 机器
每种机器有入口、出口，需要占用一定空间，有些机器还需要配套设施才能工作

铜、铁、铅、塑料、瓷、铝合金--电子厂-->电路板

更新地图-->更新生产线-->更新各仓库items-->更新UI

施工是一种特殊的生产线

总容量为所有已连通仓库之和

玩家数

允许使用的道具

流水线

原料，产品

实验室生产成本通常比工厂高