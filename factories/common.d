/// 公共模块
module factories.common;

import factories.generated;
import factories.map;
import factories.util;

/// 生产线
struct Process {
	/// 配方ID
	int recipe;
	/// 流水线乘数
	short k;
	/// 步骤
	ubyte step;
	/// 周期
	ubyte period;
}

@persist() Process[map.length] proc;

/// 设置流水线
extern (C) export Status setProc(uint slot, int recipe, uint period) {
	import factories.factory;

	if (slot >= proc.length)
		return Status.buildingNotExist;

	const level = map[slot].level;
	if (!level)
		return Status.buildingNotExist;

	proc[slot] = Process(recipe, cast(short)(1 << (level - 1)), 0, cast(ubyte)period);
	net.build();
	return Status.ok;
}

extern (C) export int getRecipe(uint slot) {
	return slot < proc.length ? proc[slot].recipe : -1;
}
