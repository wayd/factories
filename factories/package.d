module factories;

import std.meta;

public import factories.building,
factories.common,
factories.factory,
factories.game,
factories.map,
factories.storage,
factories.tile,
factories.util;

alias modules = AliasSeq!(
    factories.building,
    factories.common,
    factories.factory,
    factories.game,
    factories.map,
    factories.storage,
    factories.tile,
    factories.util);
