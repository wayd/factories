module factories.building;

import factories.generated;
import factories.storage;
import factories.util;
import factories.map;
import factories.factory;

enum {
	maxLevel = 10 // 最大建筑物等级
}

/// 放置建筑物，value的0-7位为建筑物ID，8-11位为等级，12-15位为施工/生产进度
extern (C) export Status put(int slot, uint value) {
	const building = cast(Buildings)(value & 0xff);
	if (building > Buildings.max)
		return Status.buildingNotExist;

	if (slot >= map.length)
		return Status.buildingNotExist;

	if (!onChange(slot, value))
		return Status.tooManyStorages;

	map[slot].building = building;
	map[slot].a = value >> 8 & 127 | map[slot].a & 128;
	return Status.ok;
}

unittest {
	map[0].building = Buildings.warehouse;
	map[0].level = 1;
	put(0, 288);
}
